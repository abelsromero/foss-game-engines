---
title: List of (interesting) FOSS game engine replacement projects
subtitle: by Lysol
comments: false
---

This started here https://forum.openmw.org/viewtopic.php?f=4&t=5387&sid=cdcfda43bb46900719c34fb0843621c7 

This is a simple list of interesting free open source [game engine recreations](https://en.wikipedia.org/wiki/Game_engine_recreation). What is interesting is of course very subjective, so this list is not a scientific study of the subject at all. All engines are picked purely out of subjective opinion.

So why make a list that is neither complete nor have any clear requirments for inclusion? Because [complete lists](https://en.wikipedia.org/wiki/List_of_game_engine_recreations) [already exist](https://osgameclones.com/) and because I can't think of any specific reasons for inclusion in this list other than "this project sounds cool".

Please update the status of projects if the description is outdated. See _Status last updated_ to see what might need updating.

1.   [exult](http://exult.sourceforge.net/)
     *    _Language:_ C++
     *    _Info:_ A very old project that runs Ultima VII. Is still active today it seems when I check GitHub. Should probably be fully playable, but please report here if I'm wrong.
     *    _License:_
     *    _Latest release:_
     *    _Info last updated:_ September 2018
1.   [Freeablo](https://freeablo.org/)
     *    _Language:_ C++
     *    _Info:_ Engine for Diablo 1. Seems to be active on github. Don't know about playability yet.
     *    _Licence:_
     *    _Latest release:_ 
     *    _Status last updated:_ September 2018
1.   [gemrb](http://www.gemrb.org/wiki/doku.php?id=start) – 
     *    _Language:_ C++
     *    _Info:_ This engine recreates the Infinity Engine, which is used for Baldur's Gate I & II, Planescape: Torment and others. Is fully playable already according to the website.
     *    _Licence:_
     *    _Latest release:_
     *    _Status last updated:_ September 2018
1.   [openage](https://openage.sft.mx/)
     *    _Language:_ ?
     *    _Info:_ Replacing the Age of Empires II Engine. They seem to have big plans for scripting and stuff. Active development.
     *    _Licence:_
     *    _Latest release:_
     *    _Status last updated:_ September 2018
1.   [OpenTESArena](https://github.com/afritz1/OpenTESArena) – afritz1, a member here on the forums, is working on this replacement for the engine of The Elder Scrolls: Arena. At the time of writing, latest commit was 12 hours ago, so it is under active development. (Post updated: Sept 2018)
1.   [OpenDF](https://github.com/kcat/opendf) – Runs The Elder Scrolls II: Daggerfall. Started by our very own Chris/kcat/KittyCat. Uses many of the dependencies that OpenMW uses. Slow development, but not dead. Right Chris? :) (Post updated: Sept 2018)
1.   [OpenRCT2](https://openrct2.org/) – Replacing the engine for Roller Coaster Tycoon 1 & 2\. Getting better and better for every day, so I'd say there's no reason to not use this engine. Will probably soon become what OpenTTD is to Transport Tycoon Deluxe. Don't think I need to update on this one, the engine is very mature already.
1.   [OpenRA](http://www.openra.net/) – This engine runs the classic Command & Conquer games, such as Red Alert, Tiberian Dawn and Dune2000\. They are also working on implementing support for Tiberian Sun. The engine is fully playable already. (Post updated: Sept 2018)
1.   [OpenRW](https://openrw.org/) – Supposedly short for "Open Re-Write", an engine made to run GTA III (and in the long term hopefully Vice City and San Andreas). Active development. Playable, but you can't make any significant progress in the game according to the website. (Post updated: Sept 2018)
1.   [OpenTTD](http://www.openttd.org/) – While this of course is a very simple game engine to recreate compared to OpenMW, this is still an engine to look at to know how the future might look for OpenMW. First replacing the engine for Transport Tycoon Deluxe, and later even shipping replacement for all the assets, OpenTTD is now the only serious way to play Transport Tycoon Deluxe. The engine is so far ahead of what the original engine could do. Before OpenTTD really became the real standard, there was a competing thing called TTDPatch, which was kind of what MWSE is to Morrowind. TTDPatch did a lot of good stuff for the game, but eventually OpenTTD just got better and better. The Project is pretty much finished already, but still under Active development. Won't need to update on this one.
1.   [REGoth](https://github.com/REGoth-project/REGoth) – Replacing the zEngine that runs the two first Gothic games. You can't really do anything other than run around as of yet, but otherwise it seems quite mature. Sadly, development has been dead since july, but hopefully it might resurrect again soon. (Post updated: Sept 2018)
1.   [Spring Engine](https://springrts.com/) – Originally a replacer for Total Anihillation's game Engine, but is nowadays a fully fletched RTS game engine. There are several games using the engine. Still active development on GitHub. Don't think I need to update this one either, since even if the project dies, it seems to be essentially finished. Like OpenTTD, but in another way, I'd say this gives you an idea what OpenMW _might_ become: Not just a game engine replacement, but simply a game engine.
1.   [Xoreos](https://xoreos.org/) – Replacing Bioware's Aurora Engine, for games such as Neverwinter Nights 1 & 2, Star Wars: Knights of the Old Republic 1 & 2 and more. Development seems pretty slow at the moment, but it is still active. Not really much gameplay yet. (Post updated: Sept 2018)